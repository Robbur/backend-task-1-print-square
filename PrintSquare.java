import java.util.Scanner;

public class PrintSquare {
    public static void main(String[] args) {
        int height = 0;
        int width = 0;
        Scanner s = new Scanner(System.in);

        // Get height and width from user
        System.out.println("Create your own square!");
		System.out.println("Enter a positive number for height and width!");
        System.out.print("\n");
        do {
            System.out.print("Enter height of square: ");
            //Checking if the input is an integer
            while (!s.hasNextInt()) {
                System.out.print("Please enter a number: ");
                s.next();
            }
            height = s.nextInt();
        } while (height <= 0);
        do {
            System.out.print("Enter width of square: ");
            //Checking if the input is an integer
            while (!s.hasNextInt()) {
                System.out.print("Please enter a number: ");
                s.next();
            }
            width = s.nextInt();
        } while (width <= 0);
        System.out.println("\nresult...");

        // Printing the squares
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                if ((i == 1 || i == height || j == 1 || j == width) ||
                    (i == 3 || i == height - 2 || j == 3 || j == width - 2) &&
                    (i >= 3 && i <= height - 2 && j >= 3 && j <= width - 2)) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
}